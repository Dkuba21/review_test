import datetime as dt
#
# В целом:
#  - нет тайпхинтов и, как следствие, код слабо читаем (например, если смотреть на определение методов, неясно параметры каких типов он принимает)
#  - нет докстрингов (см. PEP257)
#  - нужно привести код к PEP8 (используй например flake8)
#  - наименование непубличных атрибутов обычно именуются начиная с нижнего подчеркивания


class Record:
    def __init__(self, amount, comment, date=''):
        self.amount = amount
        self.date = (
            dt.datetime.now().date() if
            not
            date else dt.datetime.strptime(date, '%d.%m.%Y').date())
        self.comment = comment


class Calculator:
    def __init__(self, limit):
        self.limit = limit
        # для неизменяемых типов лучше использовать кортеж
        self.records = []

    def add_record(self, record):
        self.records.append(record)

    def get_today_stats(self):
        today_stats = 0

        # с верхнего регистра принято начинат наименование класса
        # переменные именуются с нижнего регистра
        for Record in self.records:
            if Record.date == dt.datetime.now().date():

                # использование оператор инкремента делает код более лаконичным
                today_stats = today_stats + Record.amount
        return today_stats

    def get_week_stats(self):
        week_stats = 0
        today = dt.datetime.now().date()
        for record in self.records:

            # Нарушается принцип DRY.
            # Лучше завести переменную и присвоить ей значение (today - record.date).days
            if (
                (today - record.date).days < 7 and
                (today - record.date).days >= 0
            ):
                week_stats += record.amount
        return week_stats


class CaloriesCalculator(Calculator):
    def get_calories_remained(self):  # Получает остаток калорий на сегодня

        # имя переменной 'x' ниочем не говорит. трудно понять что хранит переменная
        x = self.limit - self.get_today_stats()

        # else здесь не имеет смысле
        # лучше переписать код, чтобы return использовался один раз
        if x > 0:
            return f'Сегодня можно съесть что-нибудь' \
                   f' ещё, но с общей калорийностью не более {x} кКал'
        else:
            # скобки излишни
            return('Хватит есть!')


class CashCalculator(Calculator):

    USD_RATE = float(60)  # Курс доллар США.
    EURO_RATE = float(70)  # Курс Евро.

    # в верхнем регистре обозначаются константы, аргументы как и переменные
    # нужно писать в нижнем регистре
    def get_today_cash_remained(self, currency,
                                USD_RATE=USD_RATE, EURO_RATE=EURO_RATE):

        currency_type = currency
        cash_remained = self.limit - self.get_today_stats()

        # Константные значения ('usd', 'rub'...) лучше держать в константах, а не в сыром виде.
        # Нет обработки исключения (когда currency_type не равно ниодному из допустимых значений)
        # В целом if-elif лучше заменить, например, на применение словаря
        if currency == 'usd':
            cash_remained /= USD_RATE
            currency_type = 'USD'
        elif currency_type == 'eur':
            cash_remained /= EURO_RATE
            currency_type = 'Euro'
        elif currency_type == 'rub':
            # операция сравнения возвращает объект типа bool.
            # Здесь не имеет смысла.
            cash_remained == 1.00
            currency_type = 'руб'
        if cash_remained > 0:

            return (
                f'На сегодня осталось {round(cash_remained, 2)} '
                f'{currency_type}'
            )
        elif cash_remained == 0:
            return 'Денег нет, держись'
        elif cash_remained < 0:
            return 'Денег нет, держись:' \
                   ' твой долг - {0:.2f} {1}'.format(-cash_remained,
                                                     currency_type)

    def get_week_stats(self):
        # нет смысла так вызывать метод родителя, если предполагается, что методы
        # родителя и потомка идентичны
        super().get_week_stats()

        # !!! функция возвращает None
